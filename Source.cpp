#include <iostream>
#include <string>
#include <algorithm>
#include <cctype>
#include <windows.h>

// Function removes all spaces from a string
std::wstring RemoveAllSpaces(std::wstring String)
{
	String.erase(std::remove(String.begin(), String.end(), ' '), String.end());
	return String;
}

// Function checks if a string is a palindrome
bool IsPalindrome(std::wstring String)
{
	std::wstring StringWOSpaces = RemoveAllSpaces(String);
	bool Result = true,
		LengthIsEven = StringWOSpaces.length() % 2 == 0;

	std::wstring LeftPart = StringWOSpaces.substr(0, StringWOSpaces.length() / 2),
				RightPart = StringWOSpaces.substr(StringWOSpaces.length() / 2 + (LengthIsEven ? 0 : 1), StringWOSpaces.length());

	for (unsigned int i = 0; i < LeftPart.length(); i++)
	{
		if (std::tolower(LeftPart[i]) != std::tolower(RightPart[RightPart.length() - i - 1]))
		{
			Result = false;
			break;
		}
	}
	return Result;
}

// Function searches for palindromes in a string and return a list of them
std::wstring GetPalindrome(std::wstring String)
{
	std::wstring AllPalindromes (L""), CurrentPalindrome(L""),
		LeftNotAlpha (L""), RightNotAlpha (L"");
	wchar_t LeftChar = ' ', RightChar = ' ', NextRightChar = ' ';
	int LeftNotAlphaCounter = 0, RightNotAlphaCounter = 0,
		RightShift = 0;

	for (int i = 1; i < String.length(); i++)
	{
		RightChar = String[i];
		if (!iswalpha(RightChar))
		{
			continue;
		}
		for (int j = 1; i - j - LeftNotAlphaCounter >= 0 && i + j + RightNotAlphaCounter + RightShift < String.length(); j++)
		{
			LeftChar = String[i - j - LeftNotAlphaCounter];
			if (!iswalpha(LeftChar))
			{	
				LeftNotAlphaCounter++;
				LeftNotAlpha += LeftChar;
				j--;
				continue;
			}
			if (std::tolower(RightChar) != std::tolower(LeftChar))
			{
				// search next alphabetic character
				do 
				{
					RightChar = String[i + j + RightNotAlphaCounter + RightShift];
					if (!iswalpha(RightChar))
					{
						RightNotAlphaCounter++;
						RightNotAlpha += RightChar;
					}
				} 
				while (i + j + RightNotAlphaCounter + RightShift < String.length() && !iswalpha(RightChar));

				// if all characters to the right of String[i] aren't alphabetic characters
				if (!iswalpha(RightChar))
				{
					if (!CurrentPalindrome.empty())
					{
						AllPalindromes += (AllPalindromes.empty() ? L"" : L" | ") + CurrentPalindrome;
					}
					return AllPalindromes;
				}

				if (std::tolower(RightChar) != std::tolower(LeftChar))
				{
					break;
				}
			}
			else
			{
				// get NextRightChar to check later if LeftChar, RightChar and NextRightChar are all equal
				if (CurrentPalindrome.empty() && i + j < String.length())
				{
					do
					{
						NextRightChar = String[i + j + RightNotAlphaCounter];
						if (!iswalpha(NextRightChar))
						{
							RightNotAlphaCounter++;
							RightNotAlpha += NextRightChar;
						}
					}
					while (i + j + RightNotAlphaCounter < String.length() && !iswalpha(NextRightChar));
				}
			}

			// assembling found palindrome
			if (CurrentPalindrome.empty())
			{
				if (RightChar != String[i] || std::tolower(NextRightChar) == std::tolower(LeftChar))
				{
					CurrentPalindrome += String[i];
					if (std::tolower(NextRightChar) == std::tolower(LeftChar))
					{
						RightChar = NextRightChar;
					}
				}
				else
				{
					RightShift = -1;
				}
			}
			for (int k = 0; k < LeftNotAlpha.length(); k++)
			{
				if (CurrentPalindrome.empty())
				{
					CurrentPalindrome += LeftNotAlpha[k];
				}
				else
				{
					CurrentPalindrome.insert(CurrentPalindrome.cbegin(), LeftNotAlpha[k]);
				}
			}
			if (CurrentPalindrome.empty())
			{
				CurrentPalindrome += LeftChar;
			}
			else
			{
				CurrentPalindrome.insert(CurrentPalindrome.cbegin(), LeftChar);
			}
			for (int k = 0; k < RightNotAlpha.length(); k++)
			{
				CurrentPalindrome += RightNotAlpha[k];
			}
			CurrentPalindrome += RightChar;

			LeftNotAlpha = RightNotAlpha = L"";
			RightChar = NextRightChar = ' ';
		}
		if (!CurrentPalindrome.empty())
		{
			AllPalindromes += (AllPalindromes.empty() ? L"" : L" | ") + CurrentPalindrome;
			CurrentPalindrome = L"";
		}
		LeftNotAlpha = RightNotAlpha = L"";
		LeftNotAlphaCounter = RightNotAlphaCounter = RightShift = 0;
	}
	return AllPalindromes;
}

int main()
{
	std::wstring SomeString (L""), PalindromeString(L"");

	SetConsoleOutputCP(1251);
	SetConsoleCP(1251);

	std::cout << "IsPalindrome function testing\n";
	while (RemoveAllSpaces(SomeString).length() <= 1)
	{
		std::cout << "Please enter a string: ";
		std::getline(std::wcin, SomeString);
	}
	std::wcout << "String \"" << SomeString << "\" is" << (IsPalindrome(SomeString) ? "" : "n't") << " a palindrome\n";

	SomeString = L"";
	std::cout << "\nGetPalindrome function testing\n";
	while (RemoveAllSpaces(SomeString).length() <= 1)
	{
		std::cout << "Please enter a string: ";
		std::getline(std::wcin, SomeString);
	}
	PalindromeString = GetPalindrome(SomeString);
	std::wcout << "String \"" << SomeString << "\" ";
	if (PalindromeString.empty())
	{
		std::cout << "doesn't have any palindrome\n";
	}
	else
	{
		std::wcout << "has palindromes: " << PalindromeString << "\n";
	}
	return 0;
}